# Clone to ~/.dotfiles

```bash
git clone git@gitlab.com:mabarbeau1/dotfiles.git ~/.dotfiles
```

Install

```bash
sh install.sh
```
