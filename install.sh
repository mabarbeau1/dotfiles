sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash

nvm install node
npm install --global yarn git-open @vue/cli

rm ~/.gitconfig
rm ~/.zshrc

ln -s ~/.dotfiles/.gitconfig ~/.gitconfig 
ln -s ~/.dotfiles/.zshrc ~/.zshrc

(
    cd /Volumes/Code
    mkdir Personal
    mkdir Work
)

git clone git@gitlab.com:mabarbeau1/marc-my-zsh.git /Volumes/Code/Personal/marc-my-zsh
source ~/.zshrc

git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

curl -sS https://download.scdn.co/SpotifyInstaller.zip > ~/Downloads/SpotifyInstaller.zip \
    && unzip ~/Downloads/SpotifyInstaller.zip -d ~/Downloads/ \
    && rm ~/Downloads/SpotifyInstaller.zip \
    && open ~/Downloads/Install\ Spotify.app \
    && rm -r ~/Downloads/Install\ Spotify.app